main:
  addi $sp,$sp,-40
  sw $31,36($sp)
  sw $fp,32($sp)
  addi $fp,$sp, 0
  sw $4,40($fp)
  sw $5,44($fp)
  addi $5, $0, 30
  addi $4, $0, 20
  sw $4 0x0($zero)
  sw $5 0x4($zero)
  jal gcd
  sll $zero, $zero, 0

  sw $2 0x8($zero)

forever:
  beq $0, $0, forever
  sll $zero, $zero, 0

  addi $2, $0, 0
  addi $sp,$fp, 0
  lw $31,36($sp)
  lw $fp,32($sp)
  addi $sp,$sp,40
  jr $31
  sll $zero, $zero, 0

gcd:
  addi $sp,$sp,-8
  sw $fp,4($sp)
  addi $fp,$sp, 0
  sw $4,8($fp)
  sw $5,12($fp)
  lw $2,8($fp)
  sll $zero, $zero, 0
  slt $at, $2, $0
  beq $at, $zero, $L4
  sll $zero, $zero, 0
  sub $2,$0,$2

$L4:
  sw $2,8($fp)
  lw $2,12($fp)
  sll $zero, $zero, 0
  slt $at, $2, $0
  beq $at, $zero, $L5
  sll $zero, $zero, 0
  sub $2,$0,$2

$L5:
  sw $2,12($fp)
  beq $0, $0, $L6
  sll $zero, $zero, 0

$L8:
  lw $3,8($fp)
  lw $2,12($fp)
  sll $zero, $zero, 0
  slt $2,$2,$3
  beq $2,$0,$L7
  sll $zero, $zero, 0

  lw $3,8($fp)
  lw $2,12($fp)
  sll $zero, $zero, 0
  sub $2,$3,$2
  sw $2,8($fp)
  beq $0, $0, $L6
  sll $zero, $zero, 0

$L7:
  lw $3,12($fp)
  lw $2,8($fp)
  sll $zero, $zero, 0
  sub $2,$3,$2
  sw $2,12($fp)

$L6:
  lw $3,8($fp)
  lw $2,12($fp)
  sll $zero, $zero, 0
  bne $3,$2,$L8
  sll $zero, $zero, 0
  lw $2,8($fp)
  addi $sp,$fp, 0
  lw $fp,4($sp)
  addi $sp,$sp,8
  jr $31
  sll $zero, $zero, 0
