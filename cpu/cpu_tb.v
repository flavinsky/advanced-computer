module test();
    ////////////////////////////////
    // Input and wire definitions //
    ////////////////////////////////
    reg CLK;

    cpu cpu(CLK);
    
    ////////////////////////////////
    //         TB execution       //
    ////////////////////////////////
  initial begin
    $dumpfile("test.vcd");
    $dumpvars;
    CLK=0;
    #10000 $finish;
  end

  // Toggle Clock
  always #1 CLK = ~CLK;

endmodule
