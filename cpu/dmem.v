module dmem (input clk, we, input [31:0] a, wd, output [31:0] rd);

    reg [31:0] RAM[127:0];

    assign rd = RAM[a[8:2]]; // word aligned

    always@(posedge clk)
        if(we) 
            begin
                RAM[a[31:2]] <= wd;
	            $writememh ("dmem_out.dat",RAM); //stored in hexadecimal format
            end
    
    // Print out last memory location on change
    always @(RAM[0]) $display( "Time=%d,0x0000=%b,%d",$time, RAM[0],RAM[0]);
    always @(RAM[1]) $display( "Time=%d,0x0004=%b,%d",$time, RAM[1],RAM[1]);
    always @(RAM[2]) $display( "Time=%d,0x0008=%b,%d",$time, RAM[2],RAM[2]);
    always @(RAM[3]) $display( "Time=%d,0x000C=%b,%d",$time, RAM[3],RAM[3]);

endmodule
