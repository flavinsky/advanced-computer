module cpu(CLK);

input  CLK;
    
    ////////////////////////////////
    // Input and wire definitions //
    ////////////////////////////////
    reg [31:0] PC;

    wire [31:0] Instr;
    wire [31:0] READDATA;
    wire MEMWRITE;
    wire [31:0] WRITEDATA;
    wire [31:0] ALUOUT;

    wire [31:0] PCPlus4;
    wire [31:0] PCj;
    wire [31:0] PCjr;
    wire [31:0] PCPrime;
    wire [31:0] PCBranch;

    wire [31:0] Result;
    wire [31:0] RegFileAddr;

    wire [4:0] Rt;
    wire [4:0] Rd;
    wire [31:0] WriteReg;
    wire [31:0] WriteRegJal;
    
    wire [31:0] SrcA;
    wire [31:0] SrcB;
    wire [31:0] SignImm;

    wire MemToReg;
    wire MemWrite;
    wire PCSrc;
    wire [2:0] ALUcontrol;
    wire ALUSrc;
    wire RegDst;
    wire RegWrite;
    wire zero;
    wire Jump;
    wire JumpR;
    wire Jal;
    
    ////////////////////////////////
    //      Module connections    //
    ////////////////////////////////

    // module control_unit (opcode, funct, zero, PCSrc, MemToReg, ALUSrc, RegDst, RegWrite, ALUcontrol, MemWrite, Jump, JumpR, Jal);
    control_unit cu(Instr[31:26], Instr[5:0], zero, PCSrc, MemToReg, ALUSrc, RegDst, RegWrite, ALUcontrol, MemWrite, Jump, JumpR, Jal);

    // module mux21 (d0, d1, select, y);
    mux21 muxPCj(PCPlus4, {PCPlus4[31:28], Instr[25:0], 2'b0}, Jump, PCj);
    mux21 muxPCjr(PCj, SrcA, JumpR, PCjr);
    mux21 muxPCsrc(PCjr, PCBranch, PCSrc, PCPrime);
    mux21 muxA3(WriteReg, 31, Jal, WriteRegJal);
    mux21 muxWD(Result, PCPlus4+4, Jal, RegFileAddr);
    mux21 muxSrcB(WRITEDATA, SignImm, ALUSrc, SrcB);
    mux21 muxResult(ALUOUT, READDATA, MemToReg, Result);
    mux21 muxWriteReg({27'b0,Instr[20:16]}, {27'b0,Instr[15:11]}, RegDst, WriteReg); 

    // module alu_32 (SrcA, SrcB, ALUControl, Zero, ALUResult);
    alu_32 alu(SrcA, SrcB, ALUcontrol, zero, ALUOUT);
    
    // module register_file (A1, A2, A3, WD3, WE3, CLK, RD1, RD2);
    register_file regFile(Instr[25:21], Instr[20:16], WriteRegJal[4:0], RegFileAddr, RegWrite, CLK, SrcA, WRITEDATA);

    // module extender (in, out);
    extender signExt(Instr[15:0], SignImm);
    
    // Memory connections
    imem instructionMemory(PC[8:2], Instr);
    dmem dataMemory(CLK, MemWrite, ALUOUT, WRITEDATA, READDATA);

    // Incrememnt PC
    assign PCPlus4 = PC+4;
    assign PCBranch = (SignImm << 2) + PCPlus4;
  
    initial begin
      PC=0;
    end

    // Propacate new PC
    always @(posedge CLK) 
    begin
        PC <= PCPrime;
    end

endmodule
