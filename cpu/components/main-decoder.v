module main_decoder (opcode, RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal);
    input [5:0] opcode;
    output reg RegWrite;
    output reg RegDst;
    output reg ALUSrc;
    output reg [1:0] ALUOp;
    output reg Branch;
    output reg BranchNE;
    output reg MemWrite;
    output reg MemToReg;
    output reg Jump;
    output reg Jal;
    
    always @( * ) begin
        casex ( opcode )//  1        1       1       2      1        1         1         1       1     1 = 11
            6'b000000 : {RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal} <= 11'b11010000000; // ALU OP
            6'b100011 : {RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal} <= 11'b10100000100; // LW
            6'b101011 : {RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal} <= 11'b00100001000; // SW
            6'b000100 : {RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal} <= 11'b00001100000; // BE
            6'b000101 : {RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal} <= 11'b00001010000; // BNE
            6'b001000 : {RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal} <= 11'b10100000000; // ADDI
            6'b000010 : {RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal} <= 11'b00000000010; // Jump
            6'b000011 : {RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal} <= 11'b10000000011; // JAL
            default   : {RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal} <= 11'bxxxxxxxxxxx; // Default
        endcase

    end

endmodule
