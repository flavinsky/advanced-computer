module test();
  reg [31:0] reg0;
  reg [31:0] reg1;
  reg  select;
  wire [31:0] out;

  mux21 mux(reg0, reg1, select, out);

  initial begin
    $dumpfile("test.vcd");
    $dumpvars;
    reg0=0;
    reg1=8'hAA;
    select=0;
    #80 $finish;
  end

  always #20 select = ~select;

  always @(out) $display( "The value of out was changed. Time=%d, out=%b. Inputs: reg0=%b, reg1=%b, select=%b.",$time, out,reg0, reg1, select);

endmodule
