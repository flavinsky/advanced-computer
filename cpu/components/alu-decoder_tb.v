module test();
    reg [1:0] ALUOP;
    reg [5:0] FUNCT;
    wire [2:0] ALUCONTROL;
    reg [5:0] funct_data[0:5];
    reg [2:0] index;

    alu_decoder decoder(ALUOP, FUNCT, ALUCONTROL);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;
        ALUOP = 2'b10;
        FUNCT = 6'b000000;
        funct_data[0] = 6'b100000;
        funct_data[1] = 6'b100010;
        funct_data[2] = 6'b100100;
        funct_data[3] = 6'b100101;
        funct_data[4] = 6'b101010;
        funct_data[5] = 6'b000001;
        #300 $finish;
    end

    always begin
        #10;
        ALUOP = 2'b00;
		for(index = 0; index < 6; index = index + 1) begin
            #10;
		    FUNCT = funct_data[index];  
        end

        ALUOP = 2'b01;
		for(index = 0; index < 6; index = index + 1) begin
            #10;
		    FUNCT = funct_data[index];  
        end
        
        ALUOP = 2'b10;
		for(index = 0; index < 6; index = index + 1) begin
            #10;
		    FUNCT = funct_data[index];  
        end
        
        ALUOP = 2'bxx;
		for(index = 0; index < 6; index = index + 1) begin
            #10;
		    FUNCT = funct_data[index];  
        end
    end

    always @(ALUCONTROL) $display( "Time=%d, ALUControl=%b, ALUOP=%b, FUNCT=%b\n",$time, ALUCONTROL, ALUOP, FUNCT);

endmodule
