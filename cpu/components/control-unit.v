module control_unit (opcode, funct, zero, PCSrc, MemToReg, ALUSrc, RegDst, RegWrite, ALUcontrol, MemWrite, Jump, JumpR, Jal);
    input [5:0] opcode;
    input [5:0] funct;
    input zero;

    output PCSrc;
    output MemToReg;
    output ALUSrc;
    output RegDst;
    output RegWrite;
    output [2:0] ALUcontrol;
    output MemWrite;
    output Jump;
    output JumpR;
    output Jal;

    wire [1:0] ALUOp;
    wire Branch;
    wire BranchNE;
   
    main_decoder mdecoder(opcode, RegWrite, RegDst, ALUSrc, ALUOp, Branch, BranchNE, MemWrite, MemToReg, Jump, Jal);
    alu_decoder adecoder(ALUOp, funct, ALUcontrol, JumpR);

    assign PCSrc = (Branch & zero) | (BranchNE & ~zero);

endmodule
