module test();
    reg [5:0] opcode;
    wire RegWrite;
    wire RegDst;
    wire ALUSrc;
    wire [1:0] ALUOp;
    wire Branch;
    wire MemWrite;
    wire MemToReg;

    reg [5:0] opcode_data[0:5];
    reg [2:0] index;

    main_decoder decoder(opcode, RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;
        opcode_data[0] = 6'b000000;
        opcode_data[1] = 6'b100011;
        opcode_data[2] = 6'b101011;
        opcode_data[3] = 6'b000100;
        opcode_data[4] = 6'b001000;
        opcode_data[5] = 6'b111111;
        #70 $finish;
    end

    always begin
        #10;
		for(index = 0; index < 6; index = index + 1) begin
            #10;
		    opcode = opcode_data[index];  
        end
    end

    always @({RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg}) $display( "Time=%d, opcode=%b, RegWrite=%b, RegDst=%b, ALUSrc=%b, ALUOp=%b, Branch=%b, MemWrite=%b, MemToReg=%b\n",$time, opcode, RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg);

endmodule
