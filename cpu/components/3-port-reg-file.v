module register_file (A1, A2, A3, WD3, WE3, CLK, RD1, RD2);
    input [4:0] A1;
    input [4:0] A2;
    input [4:0] A3;
    input [31:0] WD3;
    input WE3;
    input CLK;

    output [31:0] RD1;
    output [31:0] RD2;

    reg [31:0] file [31:0];

    integer i;

    initial begin
        for (i=0; i<32; i++) 
            begin
                file[i] <= 32'b0;
            end 
        // Set stackpointer
        file[29] <= 32'h1fc;
    end

    assign RD1 = file[A1];
    assign RD2 = file[A2];

    always@ (posedge CLK) begin
        if(WE3 && A3)
            file[A3] <= WD3;
    end 

endmodule
