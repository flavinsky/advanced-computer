module alu_32 (SrcA, SrcB, ALUControl, Zero, ALUResult);
    input [31:0] SrcA;
    input [31:0] SrcB;
    input [2:0] ALUControl;
    
    output reg [31:0] ALUResult;
    output Zero;

    always @(SrcA, SrcB, ALUControl) begin
        case (ALUControl)
            3'b000 : ALUResult <= SrcA & SrcB;
            3'b001 : ALUResult <= SrcA | SrcB;
            3'b010 : ALUResult <= SrcA + SrcB;
            3'b011 : ALUResult <= SrcA ^ SrcB;
            3'b100 : ALUResult <= SrcA << SrcB; // Shift left
            3'b101 : ALUResult <= SrcA >> SrcB; // Shift right
            3'b110 : ALUResult <= SrcA - SrcB;
            3'b111 : ALUResult <= $signed(SrcA) < $signed(SrcB);
            default : ALUResult <= 32'b0; // Do nothing
        endcase
    end
    assign Zero = (ALUResult == 0);


endmodule
