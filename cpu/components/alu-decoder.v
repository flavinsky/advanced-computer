module alu_decoder (ALUOP, FUNCT, ALUCONTROL, JumpR);
    input [1:0] ALUOP;
    input [5:0] FUNCT;
    output reg [2:0] ALUCONTROL;
    output reg JumpR;
    
    always @( * ) begin
        casex ( {ALUOP, FUNCT} )
            8'b00xxxxxx : {ALUCONTROL, JumpR} <= 4'b0100; // LW/SW/ADDI/JUMP/JAL
            8'b01xxxxxx : {ALUCONTROL, JumpR} <= 4'b1100; // BRANCH
            8'b1x100000 : {ALUCONTROL, JumpR} <= 4'b0100; // ADD
            8'b1x100010 : {ALUCONTROL, JumpR} <= 4'b1100; // SUB
            8'b1x100100 : {ALUCONTROL, JumpR} <= 4'b0000; // AND
            8'b1x100101 : {ALUCONTROL, JumpR} <= 4'b0010; // OR
            8'b1x101010 : {ALUCONTROL, JumpR} <= 4'b1110; // SLT
            8'b1x000000 : {ALUCONTROL, JumpR} <= 4'b1000; // SLL
            8'b1x000010 : {ALUCONTROL, JumpR} <= 4'b1010; // SRL
            8'b1x001000 : {ALUCONTROL, JumpR} <= 4'b0001; // Jump Register
            default : ALUCONTROL <= 3'b000;
        endcase

    end

endmodule
