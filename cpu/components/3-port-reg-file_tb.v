module test();
    reg [4:0] A1;
    reg [4:0] A2;
    reg [4:0] A3;
    reg [31:0] WD3;
    reg WE3;
    reg CLK;
    wire [31:0] RD1;
    wire [31:0] RD2;

    register_file regfile(A1, A2, A3, WD3, WE3, CLK, RD1, RD2);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;
        CLK = 0;
        A1  = 0;
        A2  = 0;
        A3  = 0;
        WD3 = 32'hFFFFFFF0;
        WE3 = 1;
        #1500 $finish;
    end

    always begin
        #10;
        A3 =  A3+1;
        CLK = 0;
        CLK = 1; #1;
        A2 = A3;
        if (WD3 != RD2 && A3 && WE3)
            $display("Error RD2");
        A1 = A3;
        if (WD3 != RD1 && A3 && WE3)
            $display("Error RD1");
        WD3 = WD3 + 1;
    end

    always begin
       #700;
       WE3 = ~WE3;
    end 

    always @(RD1, RD2, WD3) $display( "Time=%d, A1=%d, A2=%d, A3=%d, RD1=%b RD2=%b, WD3=%b, CLK=%b, WE3=%b",$time, A1, A2, A3, RD1, RD2, WD3,CLK, WE3);

endmodule
