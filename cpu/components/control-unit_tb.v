module test();
    reg [5:0] opcode;
    reg [5:0] funct;
    reg zero;
    wire PCSrc;
    wire MemToReg;
    wire ALUSrc;
    wire RegDst;
    wire RegWrite;
    wire [2:0] ALUcontrol;
    wire MemWrite;

    reg [5:0] opcode_data[0:5];
    reg [2:0] i;
    
    reg [5:0] funct_data[0:5];
    reg [2:0] j;

    control_unit cu(opcode, funct, zero, PCSrc, MemToReg, ALUSrc, RegDst, RegWrite, ALUcontrol, MemWrite);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;
        
        opcode_data[0] = 6'b000000;
        opcode_data[1] = 6'b100011;
        opcode_data[2] = 6'b101011;
        opcode_data[3] = 6'b000100;
        opcode_data[4] = 6'b001000;
        opcode_data[5] = 6'b111111;
        
        funct_data[0] = 6'b100000;
        funct_data[1] = 6'b100010;
        funct_data[2] = 6'b100100;
        funct_data[3] = 6'b100101;
        funct_data[4] = 6'b101010;
        funct_data[5] = 6'b000001;
        zero = 0;
        #720 $finish;
    end

    always begin
		for(i = 0; i < 6; i = i + 1) begin
		    opcode = opcode_data[i];
            for(j = 0; j < 6; j = j+1) begin
                funct = funct_data[j];
                #10;
            end
        end

        // Now with branching
        zero = 1;
		for(i = 0; i < 6; i = i + 1) begin
		    opcode = opcode_data[i];
            for(j = 0; j < 6; j = j+1) begin
                funct = funct_data[j];
                #10;
            end
        end

    end

    always @({PCSrc, MemToReg, ALUSrc, RegDst, RegWrite, ALUcontrol, MemWrite}) $display( "Time=%d, opcode=%b, funct=%b, zero=%b\nPCSrc=%b, MemToReg=%b, ALUSrc=%b, RegDst=%b, RegWrite=%b, ALUcontrol=%b, MemWrite=%b\n",$time, opcode, funct, zero, PCSrc, MemToReg, ALUSrc, RegDst, RegWrite, ALUcontrol, MemWrite);
endmodule
