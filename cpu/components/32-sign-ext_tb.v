module test();
  reg [15:0] in;
  wire [31:0] out;

  extender ext(in, out);

  initial begin
    $dumpfile("test.vcd");
    $dumpvars;
    in=1;
    #320 $finish;
  end

  always #10 in = in<<1;

  always @(out) $display( "The value of out was changed. Time=%d, out=%b, in=%b",$time, out, in);

endmodule
