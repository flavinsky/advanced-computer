module test();
    reg [31:0] SrcA;
    reg [31:0] SrcB;
    reg [2:0] ALUControl;
    wire Zero;
    wire [31:0] ALUResult;

    alu_32 alu(SrcA, SrcB, ALUControl, Zero, ALUResult);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;
        SrcA  = 32'hAAAA0000;
        SrcB  = 32'h00AAAA00;
        ALUControl = 3'h0;
        #80 $finish;
    end

    always begin
        #10;
        ALUControl = ALUControl + 1;
    end

    always @(ALUResult) $display( "Time=%d, ALUControl=%b, SrcA=%b/%d, SrcB=%b/%d, ALUResult=%b/%d Zero=%b",$time, ALUControl, SrcA,SrcA, SrcB,SrcB, ALUResult,ALUResult, Zero);

endmodule
