module mux21 (d0, d1, select, y);
  input [31:0] d0;
  input [31:0] d1;
  input  select;
  output reg [31:0] y;

  always @( * ) 
  begin
      if ( select == 1)
        y <= d1;
      else
        y <= d0;
  end
endmodule
