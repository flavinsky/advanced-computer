module imem (input [6:0] a,
    output [31:0] rd);

// The "a" is the address of instruction to fetch, what
// for our purpose can be taken from ProgramCounter[7:2]

reg [31:0] RAM[127:0];
reg [7:0] j;
reg [6:0] b;


initial  begin
    $readmemh ("memfile.dat",RAM);
end

assign rd = RAM[a]; // word aligned

endmodule
