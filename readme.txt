README to single-cycle mips processor

To support the desired instruction set, a few additional signals and multiplexers had to be introduced.

 * Jump, JumpR and JAL signals to allow subroutine calls and jump back. Depending on those signals the new PC value is set depending on immediate, register or PC+4 address.

 * The jal signal is also used to store the return address in register $31.

 * Additionally the BranchNE signal was introduced to allow the branch not equal instruction.
